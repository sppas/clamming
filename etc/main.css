/*
:filename: css/main.css
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Style to be used for any SPPAS Web-based UI application.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Information:
------------

Most of the time, the font size is calculated with the global --font-size
variable. For example, this CSS is defining:
    font-size: calc(1.7 * var(--font-size));
When using "font-size: 1.7rem;" instead, the size of the font is not changed
when switching to the contrast view.

*/

/* =======================================================================
                              FONTS DEFINITIONS
  See here: https://v-fonts.com/licenses/open-source for a list of open
  source variable fonts.
  No cursive font is available.

  ======================================================================== */

@font-face {
    font-family: SansFont;
    src: url('../fonts/Commissioner.ttf') format('truetype');
}

@font-face {
    font-family: SerifFont;
    src: url('../fonts/SourceSerifPro.ttf') format('truetype');
}

@font-face {
    font-family: MonoFont;
    src: url('../fonts/FiraCode.ttf');
}

@font-face {
    font-family: PreFont;
    src: url('../fonts/ScienceGothic.ttf') format('truetype');
}

@font-face {
    font-family: SmallCapsFont;
    src: url('../fonts/ZinzinVF-nogvar.ttf') format('truetype');
}

@font-face {
    font-family: PixelFont;
    src: url('../fonts/BPdotsSquare.ttf') format('truetype');
}

/* =======================================================================
                            VARIABLE DEFINITIONS
  ======================================================================== */

/* All global variables for any screen or condition */
/* ------------------------------------------------ */

:root {
    --transition: 0.4s ease-in-out;
    --splash-height: 200px;

    /* Font definitions */
    --font-family-sans: SansFont, 'Verdana', sans-serif;
    --font-family-serif: SerifFont, 'Times New Roman', serif;
    --font-family-mono: MonoFont, 'Courier New', monospace;
    --font-family-pre: PreFont, 'Courier New', monospace;
    --font-family-pixel: PixelFont, 'Courier New', monospace;

    /* Variables overridden by "sp-contrast" or any other */
    /* -------------------------------------------------- */

    /* Font properties */
    --font-weight-thin: 150;
    --font-weight-semithin: 250;
    --font-weight-normal: 350; /* CSS3 requires to choose 300 or 400. 350 is not supported */
    --font-weight-semibold: 450;
    --font-weight-bold: 550;
    --font-weight-black: 650;
    --font-size: 16px;
    --line-height: 1.2;
    --typography-spacing-vertical: 1.2rem;
    --typography-spacing-horizontal: 0.8rem;

    /* Borders */
    --border-width: 2px;

    /* Focus */
    --focus-border-width: 2px;

    /* Table */
    --table-border-width: 2px;
}

/* Re-define some global variables for mobile screens */
/* -------------------------------------------------- */

@media (min-width: 820px) {
    :root {
        --font-size: 14px;
        --line-height: 1;
    }
}

/* Re-define some global variables for disabilities */
/* ------------------------------------------------ */

.sp-contrast {

    /* Font properties */
    --font-weight-thin: 200;
    --font-weight-semithin: 300;
    --font-weight-normal: 400;
    --font-weight-semibold: 550;
    --font-weight-bold: 700;
    --font-weight-black: 850;
    --font-size: 20px;
    --line-height: 1.4;

    /* Borders */
    --border-width: 3px;

    /* Focus */
    --focus-border-width: 3px;

    /* Table */
    --table-border-width: 3px;
}

@media (min-width: 820px) {
    :root {
        --font-size: 18px;
        --line-height: 1.1;
    }
}

/* Font properties for a local context usage */
/* ----------------------------------------- */

.sp-sans {
    font-family: var(--font-family-sans);
}

.sp-serif {
    font-family: var(--font-family-serif);
}

.sp-mono {
    font-family: var(--font-family-mono);
}

.sp-pre {
    font-family: var(--font-family-pre);
}

.sp-pixel {
    font-family: var(--font-family-pixel);
}

.sp-weight-thin {
    font-weight: var(--font-weight-thin);
}

.sp-weight-normal {
    font-weight: var(--font-weight-normal);
}

.sp-weight-bold {
    font-weight: var(--font-weight-bold);
}

.sp-weight-black {
    font-weight: var(--font-weight-black);
}

/* =======================================================================
                            HTML PAGE STYLING
  ======================================================================== */

* {
    margin: 0;
    padding: 0;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
}

*, *:before, *:after {
    box-sizing: inherit;
}

*, ::after, ::before {
    box-sizing: border-box;
    background-repeat: no-repeat
}

*:disabled {
    opacity: 0.4;
    cursor: not-allowed;
}

*[aria-disabled=true] {
    opacity: 0.4;
    cursor: not-allowed;
    overflow: unset;
}

*[aria-disabled=true]:hover,
*[aria-disabled=true]:focus {
    opacity: 0.4;
    cursor: not-allowed;
}

::after, ::before {
    text-decoration: inherit;
    vertical-align: inherit
}

html {
    width: 100%;
    height: 100%;
    scroll-behavior: smooth;
    overflow-x: hidden;
    background-color: var(--bg-color);
}

body {
    position: relative;
    cursor: default;

    background-clip: content-box;
    transition: all var(--border-color) 0.3s;
    transition-duration: 0.4s;

    -webkit-tap-highlight-color: transparent;
    -webkit-text-size-adjust: 100%;
    -moz-text-size-adjust: 100%;

    background-color: var(--bg-color);
    color: var(--fg-color);

    border-left: 1rem solid var(--logo-color);
    border-right: 1rem solid var(--logo-color);
    border-bottom: 3rem solid var(--logo-color);

    font-size: var(--font-size);
    font-family: var(--font-family-sans);
    font-weight: var(--font-weight-normal);
    font-style: normal;
    font-variant: normal;
    line-height: var(--line-height);
    text-rendering: optimizeLegibility;
    overflow-wrap: break-word;

    -moz-tab-size: 4;
    tab-size: 4;
    flex-direction: column;
}

/* =======================================================================
                     HTML DOCUMENT STRUCTURE
   ======================================================================= */

body > header {
    z-index: 10;
    text-align: center;
    padding-bottom: var(--typography-spacing-vertical);
    background: var(--header-bg-color);
    color: var(--header-fg-color);
}

body > nav {
    z-index: 10;
    background: var(--nav-bg-color);
    color: inherit;
    position: -webkit-sticky;  /* Safari */
    position: sticky;   /* is positioned based on the user's scroll position */
    top: 0;
    margin: auto;
    overflow: unset;
    /* box-shadow: [h-offset] [v-offset] [blur-radius] [spread-radius] [color]; */
    box-shadow: 0 0.3rem 0.4rem 0.2rem var(--shadow-color);
}

body > main {
    z-index: 1;
    background-color: var(--bg-color);
}

body > footer {
    z-index: 10;
    background: var(--footer-bg-color);
    color: var(--footer-fg-color);
    padding-top: var(--typography-spacing-vertical);
    padding-bottom: calc(2 * var(--typography-spacing-vertical));
    min-height: 4rem;
    box-shadow: 0 0.3rem 0.8rem 0.2rem var(--shadow-color);
}

/* Elements inside */

body > header > * {
    text-align: center;
    padding-top: var(--typography-spacing-vertical);
}

body > footer > * :is(p, h1, h2, h3, li, a) {
    color: var(--footer-fg-color);
}

/* =======================================================================
                       ACCESSIBILITY SPECIFIC STYLING
  ======================================================================== */

:is(a, video, input, area, select, button, input, textarea, summary, li) {
    --outline-offset: 0;
    --outline-size: calc(2 * var(--focus-border-width));
    --outline-style: solid;
    --outline-color: var(--focus-border-color);
}

:is(a, video, input, area, select, button, input, textarea, summary, li):focus {
    outline: var(--outline-size) var(--outline-style) var(--outline-color);
    outline-offset: var(--outline-offset, var(--outline-size));
}

:is(a, video, input, area, select, button, input, textarea, summary, li):focus-visible {
    outline: var(--outline-size) var(--outline-style) var(--outline-color);
    outline-offset: var(--outline-offset, var(--outline-size));
}

:is(a, video, input, area, select, button, input, textarea, summary, li):focus:not(:focus-visible) {
    outline: none;
}

/* A property to be assigned to a tag in case we want to force it to have
   the focus style. Example of use:
   <label class="focusable"> <input [...] /> </label>
*/

.focusable {
    border: var(--focus-border-width) solid transparent;
    border-radius: calc(2 * var(--focus-border-width));
}

.focusable:focus, .focused {
    border: var(--focus-border-width) solid var(--focus-border-color);
    border-radius: calc(2 * var(--focus-border-width));
}

/* =======================================================================
                       HTML ELEMENTS STYLING
  ======================================================================== */

main > section,
main > * section {
    padding-top: var(--typography-spacing-vertical);
    padding-bottom: var(--typography-spacing-vertical);
    padding-left: var(--typography-spacing-horizontal);
    padding-right: var(--typography-spacing-horizontal);
}

main > * article {
    margin-left: var(--typography-spacing-horizontal);
}


/* ---------------------------- Head titles ------------------------------ */

h1 {
    color: var(--h1-color);
    font: var(--font-family-sans);
    font-weight: var(--font-weight-bold);
    padding-top: calc(2 * var(--typography-spacing-vertical));
    letter-spacing: 2px;
}

body > header > h1,
body > footer > h1,
body > footer > * h1
{
    font-size: calc(2 * var(--font-size));
}

body > main > h1,
body > main > * h1
{
    text-align: center;
    font-size: calc(3 * var(--font-size));
    margin-top: calc(2 * var(--typography-spacing-vertical));
    margin-bottom: var(--typography-spacing-vertical);
    padding-bottom: var(--typography-spacing-vertical);
    border-top: 1px solid var(--logo-color);
    border-bottom: 1px solid var(--logo-color);
    line-height: calc(4 * var(--font-size));
}

h2 {
    color: var(--h2-color);
    font: var(--font-family-sans);
    font-weight: var(--font-weight-bold);
    padding-top: 1rem;
    letter-spacing: 2px;
}

body > main > h2,
body > main > * h2 {
    font-size: calc(2 * var(--font-size));
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
    padding-bottom: 2rem;
    position: relative;
}

body > main > h2:after,
body > main > * h2:after {
    position: absolute;
    left: 0;
    top: 4.5rem;
    height: 0;
    width: 20%;
    content: '';
    border-bottom: 1px solid var(--logo-color);
}

body > header > h2,
body > footer > h2
{
    font-size: calc(1.5 * var(--font-size));
}

h3 {
    font-size: calc(1.5 * var(--font-size));
    font-weight: var(--font-weight-semibold);
    color: var(--h3-color);
    margin-top: var(--typography-spacing-vertical);
    margin-bottom: var(--typography-spacing-vertical);
    letter-spacing: 2px;
}

h4 {
    margin-bottom: var(--typography-spacing-vertical);
    padding-bottom: var(--typography-spacing-vertical);
    letter-spacing: 1px;  /* For those who can't see colors */
    font-size: calc(1.3 * var(--font-size));
    color: var(--h4-color);
    font-weight: var(--font-weight-normal);
    font-style: italic;
}

h5 {
    font-style: italic;
}

h5, h6 {
    margin-bottom: var(--typography-spacing-vertical);
    font-size: var(--font-size);
    color: var(--h4-color);
    font-weight: var(--font-weight-normal);
}

/* ---------------------- Text & inline text ----------------------------- */

p {
    font: inherit;
    padding-bottom: var(--typography-spacing-vertical);
    text-align: justify;
    text-justify: inter-word;
    display: block;
}

b {
    font: inherit;
    font-weight: var(--font-weight-bold);
}

strong {
    font: inherit;
    font-weight: var(--font-weight-black);
}

mark {
    font: inherit;
    background: #ff0;
    color: #000
}

small {
    font: inherit;
    font-size: 80%
}

sub, sup {
    font: inherit;
    font-size: calc(0.75 * var(--font-size));
    line-height: 0;
    position: relative;
    vertical-align: baseline
}

sub {
    bottom: -0.25em
}

sup {
    top: -0.5em
}

kbd, pre, samp {
    font-family: var(--font-family-pre);
    font-size: calc(0.9 * var(--font-size));
}

code {
    font-family: var(--font-family-mono);
    font-size: calc(0.9 * var(--font-size));
}

kbd {
    font-weight: bolder;
}

mark {
    color: var(--mark-color);
    text-decoration: none;
}

ins {
    color: var(--ins-color);
    text-decoration: none;
}

del {
    color: var(--del-color);
}

s {
    color: var(--s-color);
}

blockquote {
    /*background-color: var(--bg-color-alt);*/
    background-image: linear-gradient(to right, var(--bg-color-alt), var(--bg-color));
    display: block;
    margin: var(--typography-spacing-vertical) 2rem var(--typography-spacing-vertical) 4rem;
    padding: calc(2*var(--typography-spacing-horizontal));
    border-right: none;
    border-left: .25rem solid var(--blockquote-border-color);
    border-inline-start: 0.25rem solid var(--blockquote-border-color);
    border-inline-end: none
}

blockquote > cite {
    background-color: transparent;
    display: block;
    margin-top: var(--typography-spacing-vertical);
    color: var(--blockquote-footer-color);
}

blockquote, blockquote > p, blockquote > a {
    font-weight: var(--font-weight-semibold);
}

a {
    color: var(--a-color);
    -webkit-transition-duration: 0.2s;
    -moz-transition-duration: 0.2s;
    -o-transition-duration: 0.2s;
    transition-duration: 0.2s;
    border: var(--focus-border-width) solid transparent;
    border-radius: calc(2 * var(--focus-border-width));
}

a:hover {
    opacity: 100%;
    border: var(--focus-border-width) dashed var(--focus-border-color);
    border-radius: calc(2 * var(--focus-border-width));
}

/* --------------------------------- List(s) ----------------------------- */

ol, ul {
    line-height: calc(1.4 * var(--line-height));
    padding: 0;
    margin-bottom: var(--typography-spacing-vertical);
    background-color: unset;
}

ul > li > ul {
    margin-bottom: 0;
}

ul > li > ul > li > ul {
    margin-bottom: 0;
}

/* Margin to right only from the 2nd level */
ul > li {
    list-style-type: none;
    list-style-position: outside;
    list-style-image: none;
}

ul > li > ul > * {
    list-style-type: none;
    list-style-position: inside;
    list-style-image: none;
}

ul:not(nav *) > li:before {
    content: "■";
    padding-right: var(--typography-spacing-horizontal);
    color: var(--logo-color);
}

ul:not(nav *) > li > ul > li:before {
    content: "◇";
    padding-right: var(--typography-spacing-horizontal);
    padding-left: var(--typography-spacing-horizontal);
    color: var(--logo-color);
}

ul:not(nav *)  > li > ul> li> ul > li:before {
    content: "□";
    padding-right: var(--typography-spacing-horizontal);
    padding-left: calc(2 * var(--typography-spacing-horizontal));
    color: var(--logo-color);
}

/* Add a dot after the item number */
ol li {
    list-style-type: none;
    counter-increment: item;
}

ol li:before {
    content: counter(item) ".";
    font-family: var(--font-family-mono);
}

/* -------------------------------- Table(s) ----------------------------- */

:where(table) {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    text-indent: 0;
    empty-cells: show;
    margin-bottom: var(--typography-spacing-vertical);
    border: var(--table-border-width) solid var(--table-border-color);
    caption-side: top;
}

td, th {
    padding: calc(var(--typography-spacing-horizontal) / 2) var(--typography-spacing-horizontal);
    border-bottom: var(--table-border-width) solid var(--table-border-color);
    border-left: var(--table-border-width) dashed var(--table-border-color);

    color: var(--fg-color);
    font-weight: var(--font-weight-normal);
    font-size: var(--font-size);
    text-align: left;
}

table[role=grid] thead {
    background-color: var(--table-head-bg-color);
    color: var(--table-head-fg-color);
    font-weight: bold;
}

tfoot td, tfoot th {
    border-top: var(--table-border-width) solid var(--table-border-color);
    border-bottom: 0;
}

table[role=grid] tbody tr:nth-child(even) {
    background-color: var(--table-row-stripped-bg-color);
}

caption {
    padding-left: calc(2 * var(--table-border-width));
    text-align: left;
    padding-top: 0.3rem;
    padding-bottom: 0.3rem;
    font-size: 80%;
    font-weight: var(--font-weight-semibold);
}

caption span {
    font-style: italic;
}

table caption {
    background-color: var(--table-caption-bg-color);
    color: var(--table-caption-fg-color);
}

/* ----------------- Button, Label and Input tags ----------------------- */

button, input, a[role=button] {
    overflow: visible;
}

button, select, a[role=button]  {
    text-transform: none;
    text-decoration: none;
}

button, [type=button], [type=reset], [type=submit], [role=button] {
    -webkit-appearance: button;
}

button, *[role=button], *[role=menuitem] {
    border: var(--focus-border-width) solid var(--buttons-color);
    border-radius: calc(3 * var(--focus-border-width));

    background-color: var(--buttons-bg-color);
    color: var(--buttons-color);

    display: inline-block;
    cursor: pointer;
    transition: all 0.4s;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    font-weight: normal;
    font-style: normal;
    font-variant: normal;
    font-size: 0.9rem;
    overflow: hidden;

    padding-top: calc(var(--typography-spacing-vertical) / 2);
    padding-right: var(--typography-spacing-horizontal);
    padding-bottom: calc(var(--typography-spacing-vertical) / 2);
    padding-left: var(--typography-spacing-horizontal);
    margin: var(--border-width);

    /* for the buttons embedded in a box */
    flex-grow: 1;      /* default is 0 */
    flex-shrink: 1;    /* default is 1 */
    flex-basis: auto;  /* default is auto */
}



button:enabled:hover, a[role=button]:hover {
    border: var(--focus-border-width) dashed var(--focus-border-color);
    opacity: 80%;
}

button:enabled img:hover {
    transform: scale(1.2);
}

.sp-contrast button {
    font-weight: bolder;
    padding-top: var(--typography-spacing-vertical);
    padding-right: calc(var(--typography-spacing-horizontal) * 2);
    padding-bottom: var(--typography-spacing-vertical);
    padding-left: calc(var(--typography-spacing-horizontal) * 2);
}

.sp-button-icon {
    margin-left: 0.3rem;
    margin-right: 0.3rem;
    max-width: 2rem;
    max-height: 2rem;
    vertical-align: middle;
}

.sp-button-text {
    font-family: inherit;
    text-align: center;
    text-decoration: none;
    vertical-align: middle;
    border-bottom: 1px dashed;
    border-color: transparent;
}

button:enabled .sp-button-text:hover {
    border-color: var(--border-color);
}

.button-icon-only {
    flex-grow: 0;
    background: transparent;
}

/* ----------------- media & figure ----------- */

figure {
    margin: auto;
    text-align: center;
}

figcaption {
    font-size: 0.9rem;
    text-align: center;
    color: var(--table-caption-bg-color);
}

video {
    margin: auto;
    max-width: 90%;
}

img {
    max-width: 100%;
}

/* ----------------- progress ----------- */

progress {
    width: 100%;
    height: 2rem;
    background: var(--progress-bg-color);
    color: var(--progress-color);
    display: block;
    position: relative;
    border: var(--border-width) solid transparent;
    border-radius: calc(2 * var(--border-width));
}

.progress-infinite {
    animation-name: animprogress;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: ease;
}

@keyframes animprogress {
    from { background-color: var(--progress-bg-color); }
    to { background-color: var(--footer-bg-color); }
}

/* ----------------- others ----------- */

hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
    border: 0;
    border-top: 1px solid rgba(128, 128, 128, 0.5);
    margin: 1rem 0
}

/* =======================================================================
                        Custom classes
   ======================================================================= */

/* --------- A simple panel, intended to be used as container.  */

.sp-panel {
    border: 0;
    padding: 0;
    margin: 0;
    position: relative;
}

.sp-panel:after, .sp-panel:before {
    content: "";
    display: table;
    clear: both
}

main.panel-item {
    padding: 0;
    margin: 0;
}

main > .panel-item:nth-child(even) {
    background-color: var(--bg-color-alt);
    color: var(--fg-color-alt);
}

/* --------- A simple panel with a flexible layout  */

.flex-panel {
    display: flex;
    justify-content: space-evenly;
}

.flex-panel > * {
    flex-grow: 0;
    flex-shrink: 1;
    flex-basis: auto;
}

/* Properties of the items in a flex panel -- if declared as it. */
.flex-item {
    margin: 0;
}

/* Properties of the columns an item is depending on the screen */
@media only screen and (min-width: 820px) {

    .flex-panel {
        flex-direction: row;
    }

    .flex-item {
        margin-right: var(--typography-spacing-horizontal);
        margin-bottom: 0;
    }

    .flex-border-left {
        border-left-style: solid;
        border-color: var(--columns-sep-color-light);
    }

}

@media only screen and (max-width: 820px) {

    .flex-panel {
        flex-direction: column;
        text-align: center;
    }

    body > footer > * > * > * > ul >li {
        list-style: none;
    }

    nav.flex-panel {
        flex-direction: row;
    }


    .flex-item {
        margin-bottom: 0;
    }

    .flex-border-left {
        border-top-style: solid;
        border-color: var(--columns-sep-color-light);
    }
}


/* --------- A simple panel with a grid layout  */

.grid-panel {
    margin: auto;
    display: grid;
    justify-content: space-evenly;
    align-items: center;
}

/* Properties of the items in a flex panel -- if declared as it. */
.grid-item {
    margin-bottom: 0;
}


/* ----------- Styling others ----------- */

.copyright {
    text-align: center;
    font-size: 80%;
    padding-top: calc(var(--typography-spacing-vertical) / 2);
    padding-bottom: calc(var(--typography-spacing-vertical) / 2);
}

.sp-grayscale {
    filter: grayscale(50%)
}

.hide {
    display: none;
}

.center {
    margin: auto;
    text-align: center;
}

.right-bottom {
    position: absolute;
    bottom: 0;
    right: 1rem;   /* room for scrollbar */
}

.left-bottom {
    position: absolute;
    bottom: 0;
    left: 1rem;   /* like with right, for symmetry */
}

.width-full {
    width: 100%;
}

.width-three-quarters {
    width: 75%;
}

.width-half {
    width: 50%;
}

.padding-button {
    padding: 1rem;
}

.logo-link{
    display: inline-block;
}

.small-logo  {
    max-height: 3.5rem;
    width: auto;
}

.medium-logo {
    max-height: 6.5rem;
    width: auto;
}

.large-logo {
    max-height: 10rem;
    width: auto;
}

.extra-large-logo {
    width: 40rem;
    min-width: 15rem;
}

.font-size {
    width: 1rem;
    height: 1rem;
}

.noborder {
    border: none
}


section.slide {
	margin-top: 5rem;
	margin-bottom: 5rem;
	background-color: var(--bg-color-alt);
    border-radius: 0.5rem;
}

/*section.slide:nth-child(even) {
	background-color: var(--bg-color);
}*/

section.slide ul,
section.slide ol
 {
	padding-left: calc(2 * var(--typography-spacing-horizontal));
}


a.external-link:after {
    position: relative;
    display: inline-flex;
    content: url("../etc/icons/external-link.png");
    width: var(--font-size);
    height: var(--font-size);
    margin-left: 0.2rem;
    margin-right: 0.2rem;
}
